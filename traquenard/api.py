from place.models import Place, GeoAdress, PlaceType
from rest_framework import viewsets, routers
from rest_framework import serializers
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from django.contrib.gis.geos import fromstr
from django.contrib.gis.measure import D
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from rest_framework.response import Response


class UserSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=100)

class PlaceTypeSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=100)
    icon = serializers.CharField(max_length=100)


class GeoAdressSerializer(serializers.Serializer):
    lon = serializers.FloatField()
    lat = serializers.FloatField()
    id = serializers.Field()

class GeoDistanceSerializer(serializers.Serializer):
    lon = serializers.FloatField()
    lat = serializers.FloatField()
    distance = serializers.FloatField()

class NearSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=100)
    address = serializers.CharField(max_length=250)
    geoadress = GeoDistanceSerializer()
    id = serializers.Field()


class PlaceSerializer(serializers.ModelSerializer):

    created_by = UserSerializer()
    place_type = PlaceTypeSerializer()
    geoadress = GeoAdressSerializer()
    user_likes = UserSerializer(source="likes", read_only=True, many=True)
    user_dislikes = UserSerializer(source="dislikes", read_only=True, many=True)
    likes = serializers.IntegerField(source='likes.count', read_only=True)
    dislikes = serializers.IntegerField(source='dislikes.count', read_only=True)
    near = NearSerializer(source = "get_places_near")
    class Meta:
        model = Place
        fields = ('name',
                  'address',
                  'place_type',
                  'description',
                  'created_by',
                  'user_likes',
                  'user_dislikes',
                  'likes',
                  'dislikes',
                  'geoadress',
                  'near',
                  'slug')
        depth = 1

class GeoadressToPlace(serializers.ModelSerializer):
    place = PlaceSerializer()
    distance = serializers.FloatField(source="get_distance")
    id = serializers.Field()
    class Meta:
        model = GeoAdress
        fields = ('place', 'distance', 'id')



class UserPreferenceSerializer(serializers.ModelSerializer):
    places_created = PlaceSerializer(many=True)
    places_liked = PlaceSerializer(many=True)
    places_disliked = PlaceSerializer(many=True)
    class Meta:
        model = User
        fields = ("username", "first_name", "last_name", "email", "places_created",
                  "places_disliked", "places_liked")
        


# ViewSets define the view behavior.
class PlaceViewSet(viewsets.ModelViewSet):
    model = Place
    serializer_class = PlaceSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = (IsAuthenticated,)


class GeoAdressViewSet(viewsets.ModelViewSet):
    model = GeoAdress
    serializer_class = GeoadressToPlace
    authentication_classes = [TokenAuthentication]
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        queryset = GeoAdress.objects.all()
        lat = self.request.QUERY_PARAMS.get("lat", None)
        lon = self.request.QUERY_PARAMS.get("lon", None)
        placetype = self.request.QUERY_PARAMS.get("placetype", None)

        if lat is not None and lon is not None:
            pnt = fromstr('POINT({0} {1})'.format(lon, lat), srid=4326)
            queryset = queryset.distance(pnt).order_by("distance")
        if placetype is not None:

            queryset = queryset.filter(place__place_type__name = placetype)
        return queryset

    def update(self, request, pk=None):
        geo = GeoAdress.objects.get(pk=pk)
        print request.DATA
        if request.DATA.get("like"):
            place = geo.place
            place.likes.add(request.user)
            place.save()

        if request.DATA.get("dislike"):
            place = geo.place
            place.dislikes.add(request.user)
            place.save()
        return super(GeoAdressViewSet, self).retrieve(request, pk)


class PlaceTypeViewSet(viewsets.ModelViewSet):
    model = PlaceType
    serializer_class = PlaceTypeSerializer 
    authentication_classes = [TokenAuthentication]
    permission_classes = (IsAuthenticated,)


class UserViewSet(viewsets.ModelViewSet):
    model = User
    serializer_class = UserPreferenceSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = (IsAuthenticated,)

    def retrieve(self, request, pk=None):
        queryset = User.objects.all()
        if pk == "me":
            user = request.user
        else:
            user = get_object_or_404(queryset, username=pk)
        serializer = UserPreferenceSerializer(user)
        return Response(serializer.data)


router = routers.DefaultRouter()
router.register(r'place', PlaceViewSet)
router.register(r'geoadress', GeoAdressViewSet)
router.register(r'placetype', PlaceTypeViewSet)
router.register(r'user', UserViewSet)
