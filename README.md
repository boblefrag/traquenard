TRAQUENARD
==========

A Django Application to manage lovely places.


COPYRIGHT
=========



 Copyright (c) 2013 Yohann Gabory

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

INSTALLATION
============

    git clone https://boblefrag@bitbucket.org/boblefrag/traquenard.git
    cd traquenard
    virtualenv env
    source env/bin/activate
    pip install -r requirements.txt
    python manage.py syncdb


If you want to populate Place Type with some examples:

    python manage.py loaddata fixtures.json

To use the search functionnality

    python manage.py rebuild_index

You should add a cron job to reindex your content for the search to
keep up to date:

    python manage.py update_index

Then run Traquenard with

    python manage.py runserver


NOTES
=====

This specials notes are for a developpement environment. For
production you should:

- serve statics by the http server, not Django
- serve site_media by the http server, not Django
- use something more robust than just sqlite3 for database connector
