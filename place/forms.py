from django import forms
from .models import Place, Image
from .osm_utils import get_geo_text

class PlaceMock(object):
    def __init__(self, address):
        self.address = address

class PlaceForm(forms.ModelForm):

    class Meta:
        model = Place
        fields = ["name", "address", "place_type", "description"]
        
    def clean(self):
        cleaned_data = super(PlaceForm, self).clean()
        obj= PlaceMock(cleaned_data.get("address"))
        if get_geo_text(obj) is None:
            raise forms.ValidationError(
                "The address field must be a real adress. Try to give a street number and a city")
        return self.cleaned_data


class ImageForm(forms.ModelForm):

    class Meta:
        model = Image
        fields = ["picture"]
