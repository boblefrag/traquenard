import datetime
from haystack import indexes
from place.models import Place


class PlaceIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    description = indexes.CharField(model_attr="description")
    created_by = indexes.CharField(model_attr='created_by')
    created_at = indexes.DateTimeField(model_attr='created_at')

    def get_model(self):
        return Place

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.filter(
            created_at__lte=datetime.datetime.now())
