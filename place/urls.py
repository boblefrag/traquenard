from django.conf.urls import patterns, url
from . import views
from haystack.views import SearchView
from haystack.forms import SearchForm

urlpatterns = patterns(
    '',
    url('^places/$',
        views.PlaceListView.as_view(),
        name="place_list"),

    url('^places/create/$',
        views.PlaceCreateView.as_view(),
        name="place_create"),

    url('^image/create/(?P<slug>\w{4}/[\w-]+)/$',
        views.ImageCreateView.as_view(),
        name="image_create"),


    url('^places/update/(?P<slug>\w{4}/[\w-]+)/$',
        views.PlaceUpdateView.as_view(),
        name="place_update"),


    url('^places/my_places/$',
        views.MyPlaceView.as_view(),
        name="my_places"),

    url('^places/by_user/(?P<username>[\w-]+)/$',
        views.UserDetailViewByUserName.as_view(),
        name="by_user"),

    url('^places/by_type/(?P<type>[\w-]+)/$',
        views.PlaceByType.as_view(),
        name="by_type"),

    url('^places/search/$',
        SearchView(form_class=SearchForm),
        name="search"),

    url('^places/like/(?P<slug>\w{4}/[\w-]+)/$',
        views.thumb_up,
        name="thumb_up"),

    url('^places/dislike/(?P<slug>\w{4}/[\w-]+)/$',
        views.thumb_down,
        name="thumb_down"),

    url('^places/(?P<slug>\w{4}/[\w-]+)/$',
        views.PlaceDetailView.as_view(),
        name="place_detail"),




    )

