from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from django.core.urlresolvers import reverse
from sorl.thumbnail import ImageField
from django.contrib.gis.db import models as geo_models
import osm_utils
from random import sample


class PlaceType(models.Model):
    """
    Define a type of place to be used by Place models.
    - cafe
    - restaurant
    - park
    - and so on
    """
    name = models.CharField(max_length=250)
    slug = models.SlugField(max_length=250)
    icon = models.CharField(max_length=250, null=True, blank=True)
    icon_type = models.CharField(max_length=250, null=True, blank=True)

    def __unicode__(self):
        return unicode(self.name)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(PlaceType, self).save(*args, **kwargs)


class Place(models.Model):
    """
    Define a Place.
    Created by a user, Liked by many user, Commented by many users
    """
    name = models.CharField(max_length=250)
    address = models.CharField(max_length=500)
    description = models.TextField()
    slug = models.SlugField(max_length=250, unique=True)
    created_by = models.ForeignKey(User, related_name="places_created")
    created_at = models.DateTimeField(auto_now_add=True)
    likes = models.ManyToManyField(User, related_name="places_liked")
    dislikes = models.ManyToManyField(User, related_name="places_disliked")
    place_type = models.ForeignKey(PlaceType)
    likes_count = models.PositiveIntegerField(default=0)
    dislikes_count = models.PositiveIntegerField(default=0)
    objects = geo_models.GeoManager()


    class Meta:
        """
        Places are ordered by most liked by default.
        """
        ordering = ['-likes_count']

    def __unicode__(self):
        return unicode(self.name)

    def get_likes_count(self):
        """
        update the like count and return it
        """
        self.likes_count = self.likes.all().count()
        self.save()
        return self.likes_count

    def get_dislikes_count(self):
        """
        update the dislikes count and return it
        """
        self.dislikes_count = self.dislikes.all().count()
        self.save()
        return self.dislikes_count

    def most_liked(self):
        """
        The Places ordered by likes count reverse (most liked to lesser liked)
        """
        return Place.objects.all().order_by("-likes_count")

    def user_places(self, user):
        """
        Filter places by created_by user
        """
        return Place.objects.filter(created_by=user)

    def user_liked_places(self, user):
        """
        The places a user likes
        """
        return Place.objects.filter(likes=user)

    def user_disliked_places(self, user):
        """
        The places a user dislikes
        """
        return Place.objects.filter(dislikes=user)

    def save(self, *args, **kwargs):
        """
        update or create the slug field before save.
        """
        super(Place, self).save(*args, **kwargs)
        chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHUJKLMNOPQRSTUVWXYZ'
        hash = ''.join(sample(chars, 4))
        self.slug = hash + '/' + slugify(self.name)
        super(Place, self).save(*args, **kwargs) #update the slug
        geo_infos = osm_utils.get_geo_text(self)
        if geo_infos:
            if GeoAdress.objects.filter(place=self).exists():
                geo = GeoAdress.objects.get(place=self)
            else:
                geo = GeoAdress() 
            geo.geom=geo_infos['geotext']
            geo.lon=geo_infos['lon']
            geo.lat=geo_infos['lat']
            geo.place = self
            geo.save()


    def get_absolute_url(self):
        """
        Return a unique url for each Place
        """
        return reverse('place_detail', args=[self.slug])

    def get_images(self):
        """
        Return the 5 last images of a Place
        """
        return self.image_set.all()[:5]

    def get_image(self):
        """
        Return the last image of a Place
        """
        return self.image_set.all()[0]


    def get_places_near(self):
        pnt = self.geoadress.geom
        geos = GeoAdress.objects.distance(pnt).exclude(place=self)[:5]
        resp = [(geo.place, geo.distance) for geo in geos]
        render = []
        for elem in resp:
            elem[0].distance = elem[1]
            render.append(elem[0])
        return render


class GeoAdress(geo_models.Model):
    place = models.OneToOneField(Place)
    geom = geo_models.GeometryField(srid=4326)
    lon = models.FloatField()
    lat = models.FloatField()
    objects = geo_models.GeoManager()

    def get_distance(self):
        if hasattr(self, "distance"):
            return self.distance.m
        else:
            return 0


class Image(models.Model):
    """
    Add images to Place
    """
    picture = ImageField(upload_to='picture/')
    place = models.ForeignKey(Place)
